from sklearn.datasets import fetch_20newsgroups
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline
from sklearn.utils import shuffle

# Choose a subset of categories in 20newsgroups to look at
categories = ['alt.atheism', 'soc.religion.christian', 'comp.graphics', 'sci.med']
# Use a built in SKLearn fcn to load the data
twenty_train = fetch_20newsgroups(subset='train', categories=categories, shuffle=True, random_state=42)
print(type(twenty_train))
# Check out how the data is stored
# First few lines of an instance
print("\n".join(twenty_train.data[1].split("\n")[:10]))
# Target is the true class (y)
# It's an integer, which matches the index of the correct target name in target_names
print(type(twenty_train.target))
print(twenty_train.target_names[twenty_train.target[0]])
# Take a look at the first ten targets, then their names
print(twenty_train.target[:10])
for t in twenty_train.target[:10]:
    print(twenty_train.target_names[t])
# Now transform these text documents into bag of words
count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(twenty_train.data)
print(X_train_counts.shape) # get back design matrix shape now with word counts 
print(count_vect.vocabulary_.get(u'algorithm'))
# Use tfidf transformer to scale counts by:
# 1) Frequency in document
# 2) Frequency of word across all documents (the)
tfidf_transformer = TfidfTransformer()
# To do the transform, give it the counts matrix and output will be scaled counts
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
print(X_train_tfidf.shape) # Notice same shape as counts matrix
# Train a NB model
clf = MultinomialNB().fit(X_train_tfidf, twenty_train.target)
docs_new = ['God is love', 'OpenGL on the GPU is fast', 'ten thousand bits']
X_new_counts = count_vect.transform(docs_new)
print(X_new_counts.shape)
X_new_tfidf = tfidf_transformer.transform(X_new_counts)
print(X_new_tfidf.shape)
predicted = clf.predict(X_new_tfidf)
for doc, cat in zip(docs_new, predicted):
    print('%r => %s' % (doc, twenty_train.target_names[cat]))
# Now instead of performing all those steps seperately, do them all at once with Pipeline
text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', MultinomialNB()),
    ])
text_clf.fit(twenty_train.data, twenty_train.target)
predicted = text_clf. predict(docs_new)
for doc, cat in zip(docs_new, predicted):
   print('%r => %s' % (doc, twenty_train.target_names[cat]))

