# Functions to prepare Imbd dataset
# COMP 551 mini project 2
# Feb 22, 2020
# Lia Formenti

from sklearn.datasets import load_files
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
from sklearn.pipeline import Pipeline

'''sklearn built in fcn.
   Assumes separate classes are in subdirs of given path.
   Returns a dict like object with the data in .data
   and the classes in .target.
   Class names can be accessed via target_names, and their
   indices give the integer representing that class.
   I did not have to specify an encoding for it to work'''
imdbTrain = load_files('aclImdb/subsetTrain/')

# Testing a subset of the training data
text_clf = Pipeline([
    ('vect', CountVectorizer()),
    ('tfidf', TfidfTransformer()),
    ('clf', MultinomialNB()),
    ])
text_clf.fit(imdbTrain.data, imdbTrain.target)

# Using my fit to predict on a second subset of training data,
# stored in subsetTest/
# The poor results are fine given that the subset is so small
imdbFakeTest = load_files('aclImdb/subsetTest/')
predicted = text_clf.predict(imdbFakeTest.data)
for i in range(len(predicted)):
    print(imdbFakeTest.target[i], predicted[i])
